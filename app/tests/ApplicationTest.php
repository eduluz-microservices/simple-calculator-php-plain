<?php

use PHPUnit\Framework\TestCase;


use \eduluz1976\Core\Application;
use \eduluz1976\Core\Constants;

class ApplicationTest extends TestCase
{
    
    /** @test */
    public function test_getInstance_should_return_a_valid_object()
    {
        $obj = Application::getInstance();

        $this->assertTrue(is_object($obj));
        $this->assertEquals(Application::class, get_class($obj));
    }

    /** @test */
    public function test_initialize_application_with_BASE_PATH()
    {
        $obj = Application::getInstance([
            Constants::APP_BASE_PATH => '/xpto'
        ], true);

        $this->assertTrue(is_object($obj));
        $this->assertEquals('/xpto', $obj->getOption(Constants::APP_BASE_PATH));
    }
}
