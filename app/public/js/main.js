
const btn = document.getElementById('btn-calculate');

const requestRemoteOperation = async () => {

    const obj = {
        x: document.getElementById('value_x').value,
        y: document.getElementById('value_y').value
    };

    const op =  document.getElementById('operation').value

    const out = document.getElementById('result');


    const rawData = await fetch('/api/v1/' + op, {
        "method": "POST",
        "body": JSON.stringify(obj)
    });
    console.log(rawData);
    const json = await rawData.json();
    return json;
}
btn.onclick = async (e) => {
    
    const resp  =  await requestRemoteOperation();
    const out = document.getElementById('result');

    out.innerHTML = resp.result; 

}

