<?php


namespace eduluz1976\Controllers;

use eduluz1976\Services\Operations as OperationsService;

class Operations
{
    public static function add($parms=[])
    {
        $post = $parms['post'];
        $result = OperationsService::add($post['x'], $post['y']);
        echo json_encode(['result'=>$result]);
    }

    public static function sub($parms=[])
    {
        $post = $parms['post'];
        $result = OperationsService::sub($post['x'], $post['y']);
        echo json_encode(['result'=>$result]);
    }

    public static function mul($parms=[])
    {
        $post = $parms['post'];
        $result = OperationsService::mul($post['x'], $post['y']);
        echo json_encode(['result'=>$result]);
    }

    public static function div($parms=[])
    {
        $post = $parms['post'];
        if ($post['y'] != 0) {
            $result = OperationsService::div($post['x'], $post['y']);
        } else {
            $result = 'Division by zero';
        }
            
        echo json_encode(['result'=>$result]);
    }
}
