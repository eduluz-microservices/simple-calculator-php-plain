<?php


use \eduluz1976\Core\Application;

$this->router->map('GET', '/', function () {
    Application::getInstance()->getView()->display('index.html');
});


$baseUri = '/api/v1';

$this->router->map('POST', $baseUri.'/add', ['\eduluz1976\Controllers\Operations','add']);
$this->router->map('POST', $baseUri.'/sub', ['\eduluz1976\Controllers\Operations','sub']);
$this->router->map('POST', $baseUri.'/mul', ['\eduluz1976\Controllers\Operations','mul']);
$this->router->map('POST', $baseUri.'/div', ['\eduluz1976\Controllers\Operations','div']);
