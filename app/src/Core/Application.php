<?php

namespace eduluz1976\Core;

class Application
{
    private static $instance;
    private $view;
    private $router;
    protected $options = [];

    public static function getInstance($options=[])
    {
        if (!static::$instance) {
            static::$instance = new Application($options);
        }
        return static::$instance;
    }


    protected function initRouter()
    {
        $this->router = new \AltoRouter();
        $this->loadRoutes();
    }


    protected function loadRoutes()
    {
        $path = $this->getOption(Constants::APP_BASE_PATH) . '/routes';

        if (\file_exists($path) && \is_dir($path)) {
            $lsFiles = \glob("$path/*.php");
            
            foreach ($lsFiles as $file) {
                include_once($file);
            }
        }
    }



    public function __construct($options=[])
    {
        $this->loadOptions($options);
        $this->initRouter();
        $this->initView();
    }


    protected function loadOptions($options)
    {
        foreach ($options as $key => $value) {
            switch ($key) {
                case Constants::APP_BASE_PATH:
                case Constants::APP_TEMPLATE_PATH:
                    $this->options[$key] = $value;
            }
        }
    }

    public function getOption($key, $default = null)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        return $default;
    }


    public function getRouter()
    {
        return $this->router;
    }


    protected function initView()
    {
        $basePath = $this->getOption(Constants::APP_TEMPLATE_PATH) ;

        $loader = new \Twig\Loader\FilesystemLoader($basePath);
        $this->view = new \Twig\Environment($loader);
    }



    public function getView()
    {
        return $this->view;
    }


    public static function run()
    {
        $match = static::getInstance()->getRouter()->match();

        // Workaround to get the posted data
        $entityBody = file_get_contents('php://input');
        $match['post'] = \json_decode($entityBody, true);

        $params = [['params' => $match['params'], 'post' => $match['post']]];


        if (is_array($match) && is_callable($match['target'])) {
            call_user_func_array($match['target'], $params);
        } elseif (is_array($match)) {
            call_user_func($match['target'], $params);
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
        }
    }
}
