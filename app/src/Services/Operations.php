<?php


namespace eduluz1976\Services;

class Operations
{
    public static function add($x, $y)
    {
        return $x+$y;
    }

    public static function sub($x, $y)
    {
        return $x-$y;
    }

    public static function mul($x, $y)
    {
        return $x*$y;
    }

    public static function div($x, $y)
    {
        return $x/$y;
    }
}
