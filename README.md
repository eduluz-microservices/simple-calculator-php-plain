# Simple Calculator in plain PHP 

This project have objective to implement a simple microservice, which will be used as template for other projects. On this case, consists in a simple calculator.

- REST API
- Containerized
- Integrated healthcheck 

This same example will be implemented in other stacks, such as:
- Laravel 8
- Phalcon 
- NodeJS
- Java Spring


