<?php

require_once __DIR__ . '/../vendor/autoload.php';

use eduluz1976\Core\Application;
use eduluz1976\Core\Constants;

error_reporting(E_ALL);

Application::getInstance([
    Constants::APP_BASE_PATH => '/srv',
    Constants::APP_TEMPLATE_PATH => '/srv/src/templates',
])->run();
